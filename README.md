# CIIDMetalMikes.MIDIScripter.Drummerer
A jam along drummer buddy AI for MainStage 3 and Logic Pro X.

## Description
- an Apple MIDI Scripter Plug-in for:
    - MainStage 3
    - Logic Pro X
- MIDI Drummer
    - collection of riffs in different genres and styles (from the Logic Pro X Drummer)
    - loop a single riff or set to random to play a different riff at the end of each cycle
    - a quick jam along buddy thats always ready to make noise!


## Installation
- create a software instrument channel strip: add a drum kit to the input or select a preset drum kit from the library
- add a MIDI scripter plug-in to a MIDI FX slot in the drummer channel strip
- click `[Open Script in Editor]`
- select all and replace the source with the contents of one of the drummer scripts in this repository (in the `/src` directory)
- click `[Run Script]` in the Script Editor
- optionally click the dropdown at the top of the MIDI Scripter window and `Save as…`
- press play `[▶️]`

## @todo 🟢 Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## @todo 🟡 Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## @todo 🟡 Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## @todo 🟡 Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## @todo 🟡 Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## @todo 🟢 License
For open source projects, say how it is licensed.

## @todo 🟡 Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
